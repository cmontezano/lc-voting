<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Idea;
use App\Models\Status;
use App\Models\User;
use App\Models\Vote;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->create([
            'name' => 'Carlos',
            'email' => 'cmontezanol@gmail.com',
        ]);

        User::factory(19)->create();

        Category::factory()->create(['name' => 'Category 1']);
        Category::factory()->create(['name' => 'Category 2']);
        Category::factory()->create(['name' => 'Category 3']);
        Category::factory()->create(['name' => 'Category 4']);

        Status::factory()->create(['name' => 'Open', 'css_classes' => 'bg-gray-200']);
        Status::factory()->create(['name' => 'Considering', 'css_classes' => 'bg-purple text-white']);
        Status::factory()->create(['name' => 'In Progress', 'css_classes' => 'bg-yellow text-white']);
        Status::factory()->create(['name' => 'Implemented', 'css_classes' => 'bg-green text-white']);
        Status::factory()->create(['name' => 'Closed', 'css_classes' => 'bg-red text-white']);

        Idea::factory(100)->create();

        // Generate unique votes. Ensure idea_id and user_id are unique for each row
        foreach (range(1, 20) as $userId) {
            foreach (range(1, 100) as $ideaId) {
                if ($ideaId % 2 === 0) {
                    Vote::factory()->create([
                        'user_id' => $userId,
                        'idea_id' => $ideaId,
                    ]);
                }
            }
        }
    }
}
